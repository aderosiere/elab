from django.contrib.staticfiles import finders
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet, ViewSet

from custom_tools.exceptions import OutputMessage
from custom_tools.permissions import IsOwnerOrStaff
import os
from django.conf import settings
from django.http import HttpResponse, Http404


class BaseElabViewSet(object):
    permission_classes = (IsAuthenticated, IsOwnerOrStaff)

    @action(detail=False, renderer_classes=[TemplateHTMLRenderer])
    def model_description(self, request):
        return Response(template_name=self.description_template)


class EquationViewSet(BaseElabViewSet, ModelViewSet):
    def get_queryset(self):
        return self.get_serializer_class().Meta.model.objects.filter(owner=self.request.user)

    @action(detail=False, methods=['post'])
    def calculate(self, request):
        serializer = self.get_serializer(data=request.data, partial=True)
        try:
            serializer.calculate()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        except OutputMessage as message:
            return Response({'exception': message.message}, status=status.HTTP_200_OK)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class StandAloneToolViewSet(BaseElabViewSet, ViewSet):
    def list(self, request):
        if settings.DEBUG:
            file_path = finders.find(self.downloadable_content)
            if os.path.exists(file_path):
                with open(file_path, 'rb') as fh:
                    response = HttpResponse(fh.read(), content_type="application/zip")
                    response['Content-Disposition'] = 'inline; filename=' + os.path.basename(file_path)
                    return response
            raise Http404
        response = HttpResponse()
        response["Content-Disposition"] = "attachment; filename=tool.zip"
        response['X-Accel-Redirect'] ="/static/"+(self.downloadable_content)
        return response
