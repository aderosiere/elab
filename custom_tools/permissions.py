from rest_framework.permissions import BasePermission


class IsOwnerOrStaff(BasePermission):
    """
    Object-level permission to allow only staff or owner of an object to edit it.
    """

    def has_object_permission(self, request, view, obj):
        return request.user.is_staff or obj.owner_id is None or obj.owner == request.user
