from django.core.exceptions import ValidationError


def validate_non_zero(value):
    if value == 0:
        raise ValidationError("This value must not be equal to 0.")


def validate_strictly_positive(value):
    if value <= 0:
        raise ValidationError("This value must be strictly positive.")
