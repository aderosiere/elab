from django.apps import AppConfig


class CustomToolsConfig(AppConfig):
    name = 'custom_tools'
