from django.contrib import admin, messages
from django.http import HttpResponse


class ExportSingleInstance(admin.ModelAdmin):
    actions = ('export_to_csv',)

    def export_to_csv(self, request, queryset):
        if queryset.count() != 1:
            self.message_user(request, "Select one single object to export.",
                              level=messages.ERROR)
            return

        instance = queryset.first()

        self.create_dataframe(instance)

        response = HttpResponse(self.dataframe.to_csv(), content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="results.csv"'

        return response

    def create_dataframe(self, instance):
        pass
