from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.core.exceptions import ValidationError
from django.db.models import (
    CharField,
    DateTimeField,
    Field,
    FloatField,
    ForeignKey,
    PositiveSmallIntegerField,
    SmallIntegerField,
    PositiveIntegerField,
    Model,
    Manager,
    BooleanField,
    CASCADE,
)


from users.models import ElabUser


class ElabField(Field):
    def __init__(
        self,
        *args,
        input=False,
        hidden=False,
        unit=None,
        alternative_units=None,
        **kwargs
    ):
        self.input = input
        self.hidden = hidden
        self.unit = unit
        if alternative_units is None:
            self.alternative_units = []
        else:
            self.alternative_units = alternative_units
        super(ElabField, self).__init__(*args, **kwargs)

    def deconstruct(self):
        name, path, args, kwargs = super(ElabField, self).deconstruct()
        for attribute in ("input", "hidden", "unit", "alternative_units"):
            kwargs[attribute] = getattr(self, attribute)
        return name, path, args, kwargs


class ElabBooleanField(ElabField, BooleanField):
    pass


class ElabFloatField(ElabField, FloatField):
    pass


class ElabPositiveIntegerField(ElabField, PositiveIntegerField):
    pass


class ElabPositiveSmallIntegerField(ElabField, PositiveSmallIntegerField):
    pass


class ElabSmallIntegerField(ElabField, SmallIntegerField):
    pass


class ElabArrayField(ElabField, ArrayField):
    pass


class BaseManager(Manager):
    def get_by_natural_key(self, owner, name):
        return self.get(owner__username=owner, name=name)


class BaseModel(Model):
    objects = BaseManager()

    owner = ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name="owner", on_delete=CASCADE, blank=True
    )
    name = CharField(verbose_name="name", max_length=60)
    created = DateTimeField(verbose_name="created", auto_now_add=True, blank=True)

    class Meta:
        abstract = True
        unique_together = ("owner", "name")

    def clean(self):
        if (
            self.pk is None
            and self._meta.model.objects.filter(owner=self.owner).count() > 4
        ):
            raise ValidationError(
                "You can only save up to five datasets. Please delete one or more "
                "datasets before saving a new one."
            )

    def natural_key(self):
        return self.owner.username, self.name
