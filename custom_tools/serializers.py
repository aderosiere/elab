from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models import NOT_PROVIDED
from rest_framework.exceptions import ValidationError
from rest_framework.fields import (
    Field,
    IntegerField,
    BooleanField,
    CharField,
    DateField,
    DateTimeField,
    DecimalField,
    EmailField,
    ModelField,
    FileField,
    FloatField,
    ImageField,
    NullBooleanField,
    SlugField,
    TimeField,
    URLField,
    IPAddressField,
    FilePathField,
    ListField,
)
from rest_framework.metadata import SimpleMetadata
from rest_framework.serializers import HyperlinkedModelSerializer

import custom_tools.models as elab_models


class ElabField(Field):
    def __init__(
        self,
        input=False,
        hidden=False,
        unit=None,
        alternative_units=None,
        size=None,
        **kwargs
    ):
        super(ElabField, self).__init__(**kwargs)
        self.input = input
        self.hidden = hidden
        self.unit = unit
        self.alternative_units = alternative_units or []
        self.size = size


class ElabBooleanField(ElabField, BooleanField):
    pass


class ElabCharField(ElabField, CharField):
    pass


class ElabDateTimeField(ElabField, DateTimeField):
    pass


class ElabIntegerField(ElabField, IntegerField):
    pass


class ElabFloatField(ElabField, FloatField):
    pass


class ElabListField(ElabField, ListField):
    pass


class EquationSerializer(HyperlinkedModelSerializer):
    serializer_field_mapping = {
        models.AutoField: ElabIntegerField,
        models.BigIntegerField: ElabIntegerField,
        models.BooleanField: BooleanField,
        models.CharField: ElabCharField,
        models.CommaSeparatedIntegerField: ElabCharField,
        models.DateField: DateField,
        models.DateTimeField: ElabDateTimeField,
        models.DecimalField: DecimalField,
        models.EmailField: EmailField,
        models.Field: ModelField,
        models.FileField: FileField,
        models.FloatField: ElabFloatField,
        models.ImageField: ImageField,
        models.IntegerField: ElabIntegerField,
        models.NullBooleanField: NullBooleanField,
        models.PositiveIntegerField: ElabIntegerField,
        models.PositiveSmallIntegerField: ElabIntegerField,
        models.SlugField: SlugField,
        models.SmallIntegerField: ElabIntegerField,
        models.TextField: ElabCharField,
        models.TimeField: TimeField,
        models.URLField: URLField,
        models.GenericIPAddressField: IPAddressField,
        models.FilePathField: FilePathField,
        elab_models.ElabBooleanField: ElabBooleanField,
        elab_models.ElabFloatField: ElabFloatField,
        elab_models.ElabPositiveIntegerField: ElabIntegerField,
        elab_models.ElabPositiveSmallIntegerField: ElabIntegerField,
        elab_models.ElabArrayField: ElabListField,
    }

    class Meta:
        read_only_fields = ("owner", "created", "url")
        extra_kwargs = {"name": {"required": False}}

    def __init__(self, *args, **kwargs):
        for chart in self.Meta.extra_kwargs.get("charts", []):
            self.default_unit_label(chart, "x")
            self.default_unit_label(chart, "y")
        super(EquationSerializer, self).__init__(*args, **kwargs)

    def build_standard_field(self, field_name, model_field):
        initial = getattr(model_field, "default", None)
        initial = None if initial == NOT_PROVIDED else initial
        input = getattr(model_field, "input", None)
        hidden = getattr(model_field, "hidden", None)
        unit = getattr(model_field, "unit", None)
        alternative_units = getattr(model_field, "alternative_units", None)
        size = getattr(model_field, "size", None)
        field_class, field_kwargs = super(
            EquationSerializer, self
        ).build_standard_field(field_name, model_field)
        field_kwargs["initial"] = initial
        field_kwargs["input"] = input
        field_kwargs["hidden"] = hidden
        field_kwargs["unit"] = unit
        field_kwargs["alternative_units"] = alternative_units
        field_kwargs["size"] = size
        return field_class, field_kwargs

    def create(self, validated_data):
        # raise ValidationError("Saving is not available for your type of account.")
        if self.Meta.model.objects.filter(owner=validated_data["owner"]).count() > 4:
            raise ValidationError(
                "You can only save up to five datasets. Please delete one or more "
                "datasets before saving a new one."
            )
        else:
            return super(EquationSerializer, self).create(validated_data)

    def validate(self, data):
        # Skip validation if name is not in the date i.e. if method calculate has been
        # called
        if "name" not in data:
            return data
        if data["name"] == "":
            raise ValidationError("Please enter a name for this data set.")
        try:
            self.Meta.model.objects.get(
                owner=self._kwargs["context"]["request"].user, name=data["name"]
            )
            raise ValidationError("You already saved a data set with this name.")
        except ObjectDoesNotExist:
            return data

    def calculate(self):
        self.is_valid(raise_exception=True)
        instance = self.Meta.model(**self.validated_data)
        instance.calculate()
        self.instance = instance

    def default_unit_label(self, chart, axis):
        """Sets the unit and/or label of the axis of the chart to
        the unit of the 1st trace field corresponding to this axis.
        """
        field_name = chart["traces"][0][axis]
        if isinstance(field_name, str):
            field = self.fields.fields[field_name]
            label = axis + "_label"
            unit = axis + "_unit"
            units = axis + "_alternative_units"
            if label not in chart and axis == "x":
                chart[label] = field.help_text
            if unit not in chart:
                chart[unit] = field.unit
            if units not in chart:
                chart[units] = field.alternative_units


class ElabMetaData(SimpleMetadata):
    def get_field_info(self, field):
        field_info = super(ElabMetaData, self).get_field_info(field)

        attributes = ("initial", "input", "hidden", "unit", "alternative_units", "size")
        for attribute in attributes:
            value = getattr(field, attribute, None)
            if value is not None and value != "":
                field_info[attribute] = value
        return field_info

    def determine_metadata(self, request, view):
        metadata = super(ElabMetaData, self).determine_metadata(request, view)
        serializer = view.get_serializer()
        try:
            metadata["charts"] = serializer.Meta.extra_kwargs["charts"]
            metadata["comparison_charts"] = serializer.Meta.extra_kwargs[
                "comparison_charts"
            ]
        except KeyError:
            pass
        return metadata
