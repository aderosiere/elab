# H2 co-volume constant (m³/kg)
b = 0.007691
g = 9.81
# adiabatic index
gamma = 1.405

# Heat of reaction of H2 (J/mol)
H_C = -244000


############################################
# Mass fractions of H2 by vol. in air (ND) #
############################################
C_ax4 = 0.002881
C_ax8 = 0.005994
C_ax11 = 0.008498
C_ax16 = 0.013037
C_ax29 = 0.0282
############################################


###############################
# Molecular masses in kg/kmol #
###############################
M_air = 29  # air
M_H2 = 2.016  # hydrogen
M_H2O = 18  # water
M_N2 = 28  # nitrogen
M_O2 = 32  # oxygen
###############################


#############################
# Specific heat ratios (ND) #
#############################
gamma_air = 1.4  # air
gamma_H2 = 1.39  # hydrogen
#############################

R = 8314.47
# H2 specific gas constant (J/(kg K))
R_H2 = R / M_H2

# Ambient pressure (Pa)
p_atm = 101325

# Critical pressure ratio (ND)
p_star = 1.89595

# Days in month
D = (31, 27, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
# Hours in month
hours_in_month = (744, 648, 744, 720, 744, 720, 744, 744, 720, 744, 720, 744)
