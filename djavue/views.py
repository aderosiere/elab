from importlib import import_module
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.reverse import reverse_lazy
from rest_framework.routers import DefaultRouter

from users.views import UserViewSet
from djavue.settings import env


router = DefaultRouter()
router.register(r'users', UserViewSet, base_name='elabuser')
TOOLS = []

for module_name in env.list('ELAB_TOOLS', default=[]):
    module = import_module('{}.views'.format(module_name))
    metadata = getattr(module, 'metadata')
    if 'options' in metadata:
        for option in metadata['options']:
            option['basename'] = option['endpoint'].replace('_', '')
            router.register(option['endpoint'],
                            option['viewset'],
                            basename=option['basename'])
            del option['viewset']
    else:
        metadata['basename'] = metadata['tool'].replace('_', '')
        router.register(metadata['tool'],
                        metadata['viewset'],
                        base_name=metadata['basename'])
        del metadata['viewset']

    TOOLS.append(metadata)


@api_view()
def toolbox_list(request):
    toolboxes = {}
    for tool in TOOLS:
        # Add the url to the tool
        if 'options' in tool:
            for option in tool['options']:
                option['url'] = reverse_lazy('{}-list'.format(option['basename']), request=request)
        else:
            tool['url'] = reverse_lazy('{}-list'.format(tool['basename']), request=request)

        # Organise tools by toolbox and section
        # Add toolbox and section in toolboxes dict if not exist
        if tool['toolbox'] not in toolboxes:
            section = []
            toolboxes[tool['toolbox']] = {tool['section']: section}
        elif tool['section'] not in toolboxes[tool['toolbox']]:
            section = []
            toolboxes[tool['toolbox']][tool['section']] = section
        else:
            section = toolboxes[tool['toolbox']][tool['section']]
        section.append(tool)

    return Response(toolboxes)


@api_view()
def tool_list(request):
    tools = []
    for tool in TOOLS:
        new_tool = tool.copy()
        if 'options' in tool:
            del new_tool['options']
            for option in tool['options']:
                tools.append({
                    **new_tool,
                    'option_title': option['title'],
                    'endpoint': option['endpoint'],
                    'url': reverse_lazy('{}-list'.format(option['basename']), request=request)
                })
        else:
            new_tool['url'] = reverse_lazy('{}-list'.format(tool['basename']), request=request)
            new_tool['endpoint'] = tool['tool']
            tools.append(new_tool)
    return Response(tools)
