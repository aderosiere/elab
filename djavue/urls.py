"""djavue URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth.views import LoginView, PasswordChangeView, PasswordChangeDoneView

from app.views import index
from djavue.views import router, tool_list, toolbox_list

urlpatterns = [
    url('^accounts/login/$',
        LoginView.as_view(),
        name='login'),
    url('^accounts/password_change/$',
        PasswordChangeView.as_view(),
        name='password_change'),
    url('^accounts/password_change/done/$',
        PasswordChangeDoneView.as_view(),
        name='password_change_done'),
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(router.urls)),
    url(r'^api/tools', tool_list),
    url(r'^api/toolboxes', toolbox_list),
    url(r'^api-auth/', include('rest_framework.urls')),
    url(r'^', index),
]
