import Vue from 'vue'
import Router from 'vue-router'

import Datasets from '../components/Datasets'
import DownloadableTool from '../components/DownloadableTool'
import Tool from '../components/Tool'
import ToolSelection from '../components/ToolSelection'
import ToolInput from '../components/ToolInput'
import ToolOutput from '../components/ToolOutput'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'tool-selection',
      component: ToolSelection
    },
    {
      path: '/integrated/:endpoint',
      component: Tool,
      children: [
        {
          path: 'input',
          component: ToolInput,
          name: 'integrated'
        },
        {
          path: 'output',
          name: 'output',
          component: ToolOutput
        },
        {
          path: 'saved_results',
          name: 'saved_results',
          component: Datasets
        }
      ]
    },
    {
      path: '/standalone/:endpoint',
      name: 'standalone',
      component: DownloadableTool
    }
  ]
})
