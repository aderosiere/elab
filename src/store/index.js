import Vue from 'vue'
import Vuex from 'vuex'
import { convert } from '../utils/unit_conversion'
import charts from './modules/charts'
import fields from './modules/fields'
import tools from './modules/tools'
import units from './modules/units'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    charts: charts,
    fields: fields,
    tools: tools,
    units: units
  },

  getters: {
    /** Returns param.value converted to the field's base unit, or the field's value converted to the selected unit of
     * the same dimension.
     * @param {string} params.fieldName - the name of the field
     * @param {(number|number[])} params.value - the value to be converted to the field's base unit. If no value is
     * passed the field's value is converted to the selected unit of the same dimension and returned
     */
    convertedValue (state) {
      return function (params) {
        let field = state.fields.fields[params.fieldName]

        // Get field's base unit
        let baseUnit = field.unit
        let value
        if (params.value !== undefined) {
          value = params.value
        } else if (field.value !== undefined) {
          value = field.value
        } else {
          return null
        }

        // Get the units to convert from and to
        let fromUnit
        let toUnit
        if (baseUnit === 'ND') {
          return value
        }
        if (baseUnit === '%') {
          if (params.value !== undefined) {
            fromUnit = '%'
          } else {
            toUnit = '%'
          }
        } else {
          if (params.value !== undefined) {
            fromUnit = state.units.dimensions[baseUnit].selectedUnit.id
            toUnit = baseUnit
          } else {
            fromUnit = baseUnit
            toUnit = state.units.dimensions[baseUnit].selectedUnit.id
          }
        }

        // Check if the conversion is to the field's base unit and if an integer is required
        let toInt = (field.type === 'integer' || (field.child && field.child.type === 'integer')) && toUnit === baseUnit
        // Convert the value (or array of values) and return it
        return convert({
          value: value,
          fromUnit: fromUnit,
          toUnit: toUnit,
          toInt: toInt
        })
      }
    },

    /** Returns param.value converted to the field's base unit.
     * @param {string} params.fieldName - the name of the field
     * @param {(number|number[])} params.value - the value to be converted
     */
    convertToSI (state, getters) {
      return params => {
        let field = state.fields.fields[params.fieldName]

        // Check if an integer is required
        let toInt = (field.type === 'integer' || (field.child && field.child.type === 'integer'))

        // Get the unit selected by the user
        let unit = getters.selectedUnit(field.unit)
        if (unit === null) {
          return params.value
        }

        // Convert to SI
        return convert({
          value: params.value,
          fromUnit: unit,
          toUnit: field.unit,
          toInt: toInt
        })
      }
    },

    /** Returns the field's value, or param.value if provided, converted to the unit selected by the user.
     * @param {string} params.fieldName - the name of the field
     * @param {(number|number[])} params.value - (Optional) the value to be converted
     */
    convertToSelectedUnit (state, getters) {
      return params => {
        let field = state.fields.fields[params.fieldName]

        // If a value is provided, use it for conversion, else use the field's value
        let value
        if (params.value !== undefined) {
          value = params.value
        } else if (field.value !== undefined) {
          value = field.value
        } else {
          return null
        }

        // Get the unit selected by the user
        let unit = getters.selectedUnit(field.unit)
        if (unit === null) {
          return params.value
        }

        // Convert to selected unit
        return convert({
          value: value,
          fromUnit: field.unit,
          toUnit: unit
        })
      }
    }
  }
})
