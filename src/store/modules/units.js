import { convert } from '@/utils/unit_conversion'

export default {
  state () {
    return {
      dimensions: {
        A: {
          label: 'Current',
          selectedUnit: {id: 'A', label: 'A'}
        },

        'A/m2': {
          label: 'Current Density',
          selectedUnit: {id: 'A/m2', label: 'A/m²'}
        },

        J: {
          label: 'Energy',
          selectedUnit: {id: 'J', label: 'J'},
          units: [
            {id: 'J', label: 'J'},
            {id: 'eV', label: 'eV'},
            {id: 'Wh', label: 'Wh'}
          ]
        },

        'J/kg': {
          label: 'Specific Energy',
          selectedUnit: {id: 'J/kg', label: 'J/kg'}
        },

        'J/kg/K': {
          label: 'Specific Energy',
          selectedUnit: {id: 'J/kg/K', label: 'J/kg/K'}
        },

        'J/mol': {
          label: 'Energy per mol',
          selectedUnit: {id: 'J/mol', label: 'J/mol'}
        },

        K: {
          label: 'Temperature',
          selectedUnit: {id: 'K', label: 'K'},
          units: [
            {id: 'K', label: 'K'},
            {id: 'degC', label: '°C'},
            {id: 'degF', label: '°F'}
          ]
        },

        'K/Pa': {
          label: 'Specific Energy',
          selectedUnit: {id: 'K/Pa', label: 'K/Pa'}
        },

        '1/K': {
          label: 'Per unit temperature',
          selectedUnit: {id: '1/K', label: '1/K'}
        },

        kg: {
          label: 'Weight',
          selectedUnit: {id: 'kg', label: 'kg'}
        },

        'kg/mol': {
          label: 'Molar Mass',
          selectedUnit: {id: 'kg/mol', label: 'kg/mol'}
        },

        'kg/m2/s': {
          label: 'Mass flux',
          selectedUnit: {id: 'kg/m2/s', label: 'kg/m²/s'}
        },

        'kg/m3': {
          label: 'Density',
          selectedUnit: {id: 'kg/m3', label: 'kg/m³'}
        },

        'kg/s': {
          label: 'Mass flow',
          selectedUnit: {id: 'kg/s', label: 'kg/s'}
        },

        W: {
          label: 'Power',
          selectedUnit: {id: 'W', label: 'W'}
        },

        m: {
          label: 'Length',
          selectedUnit: {id: 'm', label: 'm'},
          units: [
            {id: 'm', label: 'm'},
            {id: 'foot', label: 'foot'}
          ]
        },

        'm/s': {
          label: 'Speed',
          selectedUnit: {id: 'm/s', label: 'm/s'}
        },

        'm2': {
          label: 'Surface',
          selectedUnit: { id: 'm2', label: 'm²/s' },
          units: [
            { id: 'm2', label: 'm²' },
            { id: 'sqft', label: 'ft²' }
          ]
        },

        'm2/s': {
          label: 'Diffusion Coefficient',
          selectedUnit: {id: 'm2/s', label: 'm²/s'}
        },

        m3: {
          label: 'Volume',
          selectedUnit: {id: 'm3', label: 'm³'},
          units: [
            {id: 'm3', label: 'm³'},
            {id: 'liter', label: 'Liter'},
            {id: 'cuft', label: 'ft³'}
          ]
        },

        'm3/kg': {
          label: 'Specific volume',
          selectedUnit: {id: 'm3/kg', label: 'm³/kg'}
        },

        'm3/s': {
          label: 'Volume flow',
          selectedUnit: {id: 'm3/s', label: 'm³/s'}
        },

        'mol/m3': {
          label: 'Molar Concentration',
          selectedUnit: {id: 'mol/m3', label: 'mol/m³'}
        },

        Pa: {
          label: 'Pressure',
          selectedUnit: {id: 'Pa', label: 'Pa'},
          units: [
            {id: 'Pa', label: 'Pa'},
            {id: 'atm', label: 'atm'},
            {id: 'bar', label: 'bar'},
            {id: 'psi', label: 'psi'}
          ]
        },

        'Pa s': {
          label: 'Impulse',
          selectedUnit: {id: 'Pa s', label: 'Pa.s'}
        },

        'rad/s': {
          label: 'Angular Velocity',
          selectedUnit: {id: 'rad/s', label: 'rad/s'}
        },

        s: {
          label: 'Time',
          selectedUnit: {id: 's', label: 's'}
        },

        'S m2/mol': {
          label: 'Molar Conductivity',
          selectedUnit: {id: 'S m2/mol', label: 'S.m²/mol'}
        },

        V: {
          label: 'Electric potential',
          selectedUnit: {id: 'V', label: 'V'}
        },

        'V m2/A': {
          label: 'Tafel Slope',
          selectedUnit: {id: 'V m2/A', label: 'V.m²/A'}
        },

        '%': {
          label: 'Percentage',
          selectedUnit: {id: '%', label: '%'}
        },

        'ND': {
          label: 'ND',
          selectedUnit: {id: '', label: ''}
        }
      }
    }
  },

  getters: {
    convertedNumber (state) {
      return function (params) {
        return convert(params)
      }
    },

    selectedUnit (state) {
      return dimension => {
        if (['ND', '%'].indexOf(dimension) !== -1) {
          return dimension
        }
        return state.dimensions[dimension].selectedUnit.id
      }
    }
  },

  mutations: {
    selectUnit (state, selection) {
      state.dimensions[selection.dimension]['selectedUnit'] = selection.unit

      if (selection.dimension === 'J') {
        state.dimensions['J/kg']['selectedUnit'] = {
          id: selection.unit.id + '/kg',
          label: selection.unit.label + '/kg'
        }
        state.dimensions['J/kg/K']['selectedUnit'] = {
          id: selection.unit.id + '/kg/K',
          label: selection.unit.label + '/kg/K'
        }
        state.dimensions['J/kg/K']['selectedUnit'] = {
          id: selection.unit.id + '/kg/K',
          label: selection.unit.label + '/kg/K'
        }
        state.dimensions['J/mol']['selectedUnit'] = {
          id: selection.unit.id + '/mol',
          label: selection.unit.label + '/mol'
        }
      }

      if (selection.dimension === 'K') {
        state.dimensions['J/kg/K']['selectedUnit'] = {
          id: 'J/kg/' + selection.unit.id,
          label: 'J/kg/' + selection.unit.label
        }
        state.dimensions['1/K']['selectedUnit'] = {
          id: '1/' + selection.unit.id,
          label: '1/' + selection.unit.label
        }
        state.dimensions['K/Pa']['selectedUnit'] = {
          id: selection.unit.id + '/Pa',
          label: selection.unit.label + '/Pa'
        }
      }

      if (selection.dimension === 'm') {
        state.dimensions['m/s']['selectedUnit'] = {
          id: selection.unit.id + '/s',
          label: selection.unit.label + '/s'
        }
      }

      if (selection.dimension === 'm2') {
        state.dimensions['A/m2']['selectedUnit'] = {
          id: 'A/' + selection.unit.id,
          label: 'A/' + selection.unit.label
        }
        state.dimensions['kg/m2/s']['selectedUnit'] = {
          id: 'kg/' + selection.unit.id + '/s',
          label: 'kg/' + selection.unit.label + '/s'
        }
        state.dimensions['m2/s']['selectedUnit'] = {
          id: selection.unit.id + '/s',
          label: selection.unit.label + '/s'
        }
        state.dimensions['S m2/mol']['selectedUnit'] = {
          id: 'S ' + selection.unit.id + '/mol',
          label: 'S.' + selection.unit.label + '/s'
        }
        state.dimensions['V m2/A']['selectedUnit'] = {
          id: 'V ' + selection.unit.id + '/A',
          label: 'V.' + selection.unit.label + '/A'
        }
      }

      if (selection.dimension === 'm3') {
        state.dimensions['kg/m3']['selectedUnit'] = {
          id: 'kg/' + selection.unit.id,
          label: 'kg/' + selection.unit.label
        }
        state.dimensions['mol/m3']['selectedUnit'] = {
          id: 'mol/' + selection.unit.id,
          label: 'mol/' + selection.unit.label
        }
        state.dimensions['m3/kg']['selectedUnit'] = {
          id: selection.unit.id + '/kg',
          label: selection.unit.label + '/kg'
        }
        state.dimensions['m3/s']['selectedUnit'] = {
          id: selection.unit.id + '/s',
          label: selection.unit.label + '/s'
        }
      }

      if (selection.dimension === 'Pa') {
        state.dimensions['K/Pa']['selectedUnit'] = {
          id: 'K/' + selection.unit.id,
          label: 'K/' + selection.unit.label
        }
        state.dimensions['Pa s']['selectedUnit'] = {
          id: selection.unit.id + ' s',
          label: selection.unit.label + '.s'
        }
      }
    }
  }
}
