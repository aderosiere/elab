export default {
  state () {
    return {}
  },

  getters: {
    convertTo (state) {
      return function (unit) {
        return 0
      }
    },

    baseValue (state) {
      return state.getters.convertTo(state.baseUnit)
    }
  },

  mutations: {
    convertTo (state, unit) {
      return 0
    },

    init (state, params) {
      state.baseUnit = params.baseUnit
      state.unit = params.unit
      if (params.initial) {
        state.value = math.eval(params.initial)
      }
    },

    setValue (state, value) {
      state.value = value
    }
  }
}