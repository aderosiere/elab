import Vue from 'vue'
import _ from 'lodash'

import { fieldFactory } from '@/utils/field'

export default {
  state () {
    return {
      datasets: [],
      fields: {}
    }
  },

  getters: {
    /**
     * Returns false if the fields are not yet defined (by checking if field 'url' is defined)
     * or if one of the output fields value is null, true otherwise
     * @param state
     * @returns {boolean}
     */
    calculationProcessed (state) {
      if (state.datasets.length === 0 || state.datasets[0].url === undefined) {
        return false
      }
      let calculationProcessed = true
      _.forOwn(state.fields, (field, name) => {
        if (
          !field.input &&
          state.datasets[0][name].value === null
        ) {
          calculationProcessed = false
          return false
        }
      })
      return calculationProcessed
    },

    getField (state, fieldName) {
      return state.fields[fieldName]
    },

    model (state) {
      let model = {}
      _.forOwn(state.fields, (field, name) => {
        if (field.input && field.value !== null) {
          model[name] = field.value
        }
      })
      return model
    },

    result (state) {
      let result = {}
      _.forOwn(state.fields, function (field, name) {
        result[name] = field.value
      })
      return result
    }
  },

  mutations: {
    addFields (state, fields) {
      const skip = ['url', 'owner', 'name', 'created', 'option']
      let newFields = {}
      Object.keys(fields).forEach(fieldName => {
        if (skip.includes(fieldName) || fields[fieldName].type === 'string') {
          return
        }
        newFields[fieldName] = fieldFactory(fields[fieldName])
      })
      state.fields = newFields
    },

    // Reset the values to the input values given by the user
    changeInputs (state) {
      _.forOwn(state.fields, (field, fieldName) => {
        if (field.input) {
          let value = state.datasets[0][fieldName]
          // If the value is an array but the input field is a single number,
          // use the first item of the array
          if (value.length && !field.size) {
            value = [value[0]]
          }
          field.setValues(value)
        }
      })
    },

    removeFields (state) {
      state.fields = {}
    },

    resetFields (state) {
      _.forOwn(state.fields, field => {
        if (field.input) {
          field.reset()
        }
      })
    },

    setDatasetName (state, name) {
      Vue.set(state.datasets[0], 'name', name)
    },

    setDatasets (state, datasets) {
      state.datasets = datasets
    }
  }
}
