
export default {
  state () {
    return {
      tools: [],
      options: [],
      sections: [],
      selectedSections: [],
      filteredTools: []
    }
  },

  getters: {
    toolOption (state) {
      return endpoint => {
        return state.options.find(option => {
          return option.endpoint === endpoint
        })
      }
    },

    toolSection (state) {
      return endpoint => {
        return state.sections.find(section => {
          return section.endpoint === endpoint
        })
      }
    }
  },

  mutations: {
    setTools (state, options) {
      state.options = options

      // List sections
      options.forEach(option => {
        if (state.sections.indexOf(option.section) === -1) {
          state.sections.push(option.section)
        }
      })
    },
    filterTools (state, selectedSections) {
      let tools = {}
      state.options.forEach(option => {
        if (selectedSections.indexOf(option.section) !== -1) {
          if (!tools[option.tool]) {
            tools[option.tool] = {
              title: option.title,
              options: []
            }
          }
          tools[option.tool].options.push(option)
        }
      })
      state.filteredTools = tools
    }
  }
}
