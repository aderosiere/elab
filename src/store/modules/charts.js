export default {
  state () {
    return {
      charts: [],
      comparison_charts: []
    }
  },

  getters: {
    comparisonFields (state) {
      let fields = []
      state.comparison_charts.forEach(chart => {
        if (typeof chart.trace.x === 'string') {
          fields.push(chart.trace.x)
        }
        fields.push(chart.trace.y)
      })
      return fields
    }
  },

  mutations: {
    setCharts (state, data) {
      state.charts = data.charts || []
      state.comparison_charts = data.comparison_charts || []
    }
  }
}
