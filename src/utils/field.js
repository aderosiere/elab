import math from 'mathjs'

import { conversion, label, latex } from './unit_conversion'
import units from '../assets/units'

export class Field {
  constructor (field) {
    this.help_text = field.help_text
    this.label = field.label
    this.hidden = field.hidden

    if (field.type === 'list') {
      this.type = field.child.type
      this.isList = true
    } else {
      this.type = field.type
      this.isList = false
    }

    if (field.unit && field.unit !== 'ND') {
      this.unit = field.unit

      if (!field.alternative_units) {
        this.units = [{
          id: this.unit,
          label: label(this.unit),
          latex: latex(this.unit)
        }]
      } else if (field.alternative_units === 'all') {
        // Find all the alternative units
        this.units = Object.values(units).find(alternatives => (
          alternatives.map(a => a.id).includes(field.unit)
        ))
      } else {
        field.alternative_units.push(this.unit)
        this.units = field.alternative_units.map(u => (
          { id: u, label: label(u), latex: latex(u) }
        ))
      }

      this.selected_unit = field.unit
    }
  }

  changeUnit (unit) {
    this.selected_unit = unit
  }

  convert (value, fromUnit, toUnit) {
    let thisConversion = conversion({
      fromUnit: fromUnit,
      toUnit: toUnit,
      toInt: this.type === 'integer'
    })
    if (Array.isArray(value)) {
      return value.map(thisConversion)
    }
    return thisConversion(value)
  }

  convertToBase (value) {
    let thisConversion = conversion({
      fromUnit: this.selected_unit,
      toUnit: this.unit,
      toInt: this.type === 'integer'
    })
    if (Array.isArray(value)) {
      return value.map(thisConversion)
    }
    return thisConversion(value)
  }

  convertToSelected (value) {
    let thisConversion = conversion({
      fromUnit: this.unit,
      toUnit: this.selected_unit,
      toInt: this.type === 'integer'
    })
    if (Array.isArray(value)) {
      return value.map(thisConversion)
    }
    return thisConversion(value)
  }

  fullLabel () {
    return this.label + ' (' + this.unitLabel() + ')'
  }

  symbolAndUnit () {
    return this.symbolAndOtherUnit(this.unitLatex())
  }

  symbolAndOtherUnit (unitLabel) {
    /** surrounding '$'s are removed and the full symbol + unit are wrapped
     * again in '$' so the whole string is interpreted as a LaTex expression.
     */
    let symbol = this.help_text
    if (symbol.charAt(0) === '$') {
      symbol = symbol.substring(1, symbol.length - 1)
    }
    return `$${symbol} \\: (${unitLabel})$`
  }

  unitLabel () {
    if (this.unit) {
      return this.units.find(unit => unit.id === this.selected_unit).label
    }
    return ''
  }

  unitLatex () {
    if (this.unit) {
      return this.units.find(unit => unit.id === this.selected_unit).latex
    }
    return ''
  }
}

export class InputField extends Field {
  constructor (field) {
    super(field)
    this.required = field.required
    this.initial = field.initial
    this.input = true
    this.max_value = field.max_value
    this.min_value = field.min_value
    this.size = field.size
    this.value = field.initial || null

    this.converted = {
      max_value: this.max_value,
      min_value: this.min_value,
      initial: this.initial,
      value: this.initial
    }
  }

  changeUnit (unit) {
    this.selected_unit = unit
    let conversion = x => math.eval(x + ' ' + this.unit + ' to ' + this.selected_unit).toNumber()
    if (this.max_value) {
      this.converted['max_value'] = conversion(this.max_value)
    }
    if (this.min_value) {
      this.converted['min_value'] = conversion(this.min_value)
    }
    if (this.initial) {
      if (Array.isArray(this.initial)) {
        this.converted['initial'] = this.initial.map(conversion)
      } else {
        this.converted['initial'] = conversion(this.initial)
      }
    }
    if (Array.isArray(this.value)) {
      this.converted['value'] = this.value.map(conversion)
    } else {
      this.converted['value'] = conversion(this.value)
    }
  }

  reset () {
    this.converted.value = this.converted.initial
    this.setValue(this.initial)
  }

  setValues (value) {
    this.converted.value = this.convertToSelected(value)
    this.setValue(value)
  }

  setValue (value) {
    this.value = value
  }

  setValueFromConverted (value) {
    if (this.selected_unit === this.unit) {
      this.setValue(value)
    } else {
      this.setValue(this.convertToBase(value))
    }
  }
}

export function fieldFactory (field) {
  if (field.input) {
    return new InputField(field)
  }
  return new Field(field)
}
