import math from 'mathjs'
import unitSymbols from '../assets/unit_symbols'

math.createUnit('euro')

export function convert (params) {
  let conversion
  let nonConvertibles = ['ND']
  // If the unit is not known to Math.js and there is no alternative unit available anyway,
  // return the value directly
  if (
    nonConvertibles.indexOf(params.fromUnit) !== -1 ||
    nonConvertibles.indexOf(params.toUnit) !== -1 ||
    params.fromUnit === params.toUnit
  ) {
    conversion = x => x
  } else if (params.fromUnit === '%') {
    conversion = x => x / 100
  } else if (params.toUnit === '%') {
    conversion = x => x * 100
  } else {
    conversion = x => math.eval(x + ' ' + params.fromUnit + ' to ' + params.toUnit).toNumber()
  }

  let func
  // If an integer is requested, round the value
  if (params.toInt) {
    func = x => Math.round(conversion(x))
  } else {
    func = x => conversion(x)
  }
  // If the input value is an array, apply the conversion to all items
  if (Array.isArray(params.value)) {
    return params.value.map(func)
  }
  return func(params.value)
}

export function conversion (params) {
  let conversion
  let nonConvertibles = ['ND']
  // If the unit is not known to Math.js and there is no alternative unit available anyway,
  // return the value directly
  if (
    nonConvertibles.indexOf(params.fromUnit) !== -1 ||
    nonConvertibles.indexOf(params.toUnit) !== -1 ||
    params.fromUnit === params.toUnit
  ) {
    conversion = x => x
  } else if (params.fromUnit === '%') {
    conversion = x => x / 100
  } else if (params.toUnit === '%') {
    conversion = x => x * 100
  } else {
    conversion = x => math.eval(x + ' ' + params.fromUnit + ' to ' + params.toUnit).toNumber()
  }

  // If an integer is requested, round the value
  if (params.toInt) {
    return x => Math.round(conversion(x))
  }
  return conversion
}

function symbol (unit, format) {
  let multipliedUnits = unit.split(' ')
  let multipliedSymbols = []
  let multiplicationSymbol
  let notations
  if (format === 'latex') {
    multiplicationSymbol = '\\cdot'
    notations = unitSymbols.latex
  } else {
    multiplicationSymbol = ' '
    notations = unitSymbols.labels
  }
  multipliedUnits.forEach(mu => {
    let baseUnits = mu.split('/')
    let symbols = baseUnits.map(u => (notations[u] || u))
    multipliedSymbols.push(symbols.join('/'))
  })
  return multipliedSymbols.join(multiplicationSymbol)
}

/**
 * Returns the human-readable string for the given unit.
 * ex: 'euro/m3' => '€/m³'
 * @param {string!} unit The math.js-readable string representing the unit
 * @returns {string} The human-readable label of the unit
 */
export function label (unit) {
  return symbol(unit, 'text')
}

/**
 * Returns the latex string for the given unit.
 * ex: 'euro/m3' => '€/m³'
 * @param {string!} unit The math.js-readable string representing the unit
 * @returns {string} The latex notation of the unit
 */
export function latex (unit) {
  return symbol(unit, 'latex')
}
