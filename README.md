# eLab

> A hydrogen e-laboratory for e-learning.

[Documentation](https://gitlab.com/net-tools/elab/wikis/home)


## Disclaimer

This document is intended to provide the core instructions and guidelines to contribute to the eLab project. eLab is built upon popular web frameworks and technologies. Details on how these work is out of the scope of this document, but please refer to their own documentation for more information.

The bash commands provided in this document have been tested on an Ubuntu system. They are provided as a reference and should work on various systems, provided you have a bash shell. But it is up to you to perform the same operations with other tools, GUIs, or whatever is more convenient to you.


## Prerequisites

- Python 3
  - [Installation instructions](https://realpython.com/installing-python/)
- pip (Python package management)
  - [Installation instructions](https://www.makeuseof.com/tag/install-pip-for-python/)
  - You should create a Python virtual environment for this project to isolate it from your Python global installation. It is recommended to use virtualenv tools for convenience, such as [Pew](https://github.com/berdario/pew/) (which is used is this documentation).
- PostgreSQL
  - [Installation instructions for Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-postgresql-on-ubuntu-18-04)
- Node.js
  - [Installation instructions](https://www.taniarascia.com/how-to-install-and-use-node-js-and-npm-mac-and-windows/)
- Git
  - [Installation instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
  - This project uses the [git flow](https://nvie.com/posts/a-successful-git-branching-model/) branching model, it is recommended to learn it prior to working on this project.
  
 The installation instructions above are provided only for convenience. How to install the prerequisites is up to you.


## Getting started

Clone the repository, and checkout the `develop` branch:

```bash
git clone https://gitlab.com/net-tools/elab.git
git checkout develop
```

### Back-end

1. Create a Python 3 virtual environment for the project and install all the Python dependencies.

    ```bash
    pew new -p /usr/bin/python3 -a /path/to/project/ -r /path/to/project/requirements.txt elab
    ```

2. Create a database for the project.

    ```bash
    psql
    username=# create database elab;
    ```

3. Create a .env file in `/djanvue/.env` for local settings. Here is an example:

    ```
    DEBUG=on
    SECRET_KEY=lotsofweirdcharacters
    DATABASE_URL=postgres://username:password@localhost/elab
    ALLOWED_HOSTS=*
    ```

4. Apply the Django migrations to the database (this will create all the tables).

    ```bash
    python manage.py migrate
    ```

5. Create a superuser.

    ```bash
    python manage.py createsuperuser
    # Then follow instructions on terminal
    ```

6. Now you should be able to run the Django development server.

    ```bash
    python manage.py runserver
    ```
    
You can check that the Django developement server is working by visiting http://localhost:8000/api/ (or whatever URL is specified in the terminal).


### Front-end

1. Install all the JavaScript dependencies.

    ```bash
    npm install
    ```

2. Then run the node development server:

    ```bash
    npm run dev
    ```

3. And visit http://localhost:8080 (or whatever URL is specified in the terminal) to check that the frontend development server is also working.


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
