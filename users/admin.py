from django.contrib.admin import register
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import get_user_model

from users.models import ElabUser


@register(ElabUser)
class ElabUserAdmin(UserAdmin):
    fieldsets = UserAdmin.fieldsets + (('Additional', {'fields': ('role', 'organisation')}),)
    list_display = UserAdmin.list_display + ('organisation', 'role',)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if request.user.role == ElabUser.ORG_ADMIN:
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions

    def get_fieldsets(self, request, obj=None):
        if obj is not None and request.user.role == ElabUser.ORG_ADMIN:
            return (
                (None, {'fields': ('username', 'password', 'is_active')}),
                ('Personal info', {'fields': ('email', 'first_name', 'last_name')}),
            )
        return super(ElabUserAdmin, self).get_fieldsets(request, obj)

    def get_queryset(self, request):
        if request.user.role == ElabUser.ORG_ADMIN:
            return get_user_model().objects.filter(organisation=request.user.organisation)
        return get_user_model().objects.all()

    def has_delete_permission(self, request, obj=None):
        if obj is None:
            return True
        if request.user.role == obj.role == ElabUser.ORG_ADMIN:
            return False
        return True

    def save_model(self, request, obj, form, change):
        if not change and request.user.role == ElabUser.ORG_ADMIN:
            obj.organisation = request.user.organisation
            obj.role = ElabUser.STUDENT
        super().save_model(request, obj, form, change)
