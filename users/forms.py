from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from users.models import ElabUser


class ElabUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = ElabUser
        fields = UserCreationForm.Meta.fields + ('organisation', 'role')


class ElabUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = ElabUser
        fields = '__all__'
