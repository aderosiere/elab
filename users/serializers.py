from django.conf import settings
from rest_framework.serializers import HyperlinkedModelSerializer

from users.models import ElabUser


class UserSerializer(HyperlinkedModelSerializer):
    class Meta:
        # model = settings.AUTH_USER_MODEL
        model = ElabUser
        fields = ('url', 'username')
