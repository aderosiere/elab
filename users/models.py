from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.db.models import CharField


class ElabUser(AbstractUser):
    ORG_ADMIN = 'org_admin'
    STUDENT = 'student'
    ROLES = ((ORG_ADMIN, 'Organisation administrator'),
             (STUDENT, 'Student'))

    role = CharField(choices=ROLES, max_length=50, blank=True)
    organisation = CharField(max_length=100, blank=True)

    class Meta:
        verbose_name = 'user'

    def clean(self):
        if self.role == self.ORG_ADMIN:
            if self.organisation == '':
                raise ValidationError('Field organisation cannot be blank for administrators.')
            self.is_staff = True
        elif self.role == self.STUDENT:
            self.is_staff = False
