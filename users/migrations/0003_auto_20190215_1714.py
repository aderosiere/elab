# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2019-02-15 16:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20181106_0955'),
    ]

    operations = [
        migrations.AlterField(
            model_name='elabuser',
            name='organisation',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
