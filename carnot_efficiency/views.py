from carnot_efficiency.serializers import CarnotEfficiencySerializer
from custom_tools.views import EquationViewSet


class CarnotEfficiencyViewSet(EquationViewSet):
        serializer_class = CarnotEfficiencySerializer
        description_template = 'carnot_efficiency/carnot_efficiency_description.html'


metadata = {
        'toolbox': 'engineering',
        'section': 'test',
        'tool': 'carnot_efficiency',
        'title': 'Carnot efficiency',
        'type': 'integrated',
        'viewset': CarnotEfficiencyViewSet,
}
