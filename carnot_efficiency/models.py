from django.core.validators import MinValueValidator, MaxValueValidator

from custom_tools.exceptions import OutputMessage
from custom_tools.models import BaseModel, ElabFloatField


class CarnotEfficiency(BaseModel):
    # Inputs
    T_h = ElabFloatField(verbose_name="Temperature hot reservoir",
                         help_text="$T_h$",
                         input=True,
                         default=378,
                         unit='K',
                         alternative_units=['degC'],
                         validators=[MinValueValidator(0), MaxValueValidator(1273)])
    T_c = ElabFloatField(verbose_name="Temperature cold reservoir",
                         help_text="$T_c$",
                         input=True,
                         default=293,
                         unit='K',
                         validators=[MinValueValidator(0), MaxValueValidator(500)])
    C_eff = ElabFloatField(verbose_name="Carnot efficiency",
                           help_text="$T_c$",
                           input=False,
                           default=0)

    def calculate(self):
        if self.T_h - self.T_c <= 0:
            raise OutputMessage("T hot - T cold must be strictly positive.")
        self.C_eff = (self.T_h - self.T_c) / self.T_h
