from custom_tools.serializers import EquationSerializer
from carnot_efficiency.models import CarnotEfficiency


class CarnotEfficiencySerializer(EquationSerializer):
    class Meta(EquationSerializer.Meta):
        model = CarnotEfficiency
        fields = ('url', 'name', 'created', 'T_h', 'T_c', 'C_eff')
