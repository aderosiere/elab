from django.apps import AppConfig


class CarnotEfficiencyConfig(AppConfig):
    name = 'carnot_efficiency'
